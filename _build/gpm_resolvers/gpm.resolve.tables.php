<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package forestbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/') . 'model/';
            
            $modx->addPackage('forestbrain', $modelPath, null);


            $manager = $modx->getManager();

            $manager->createObjectContainer('forestData');
            $manager->createObjectContainer('forestFeature');
            $manager->createObjectContainer('forestNeed');
            $manager->createObjectContainer('forestComponent');
            $manager->createObjectContainer('forestNeedComp');
            $manager->createObjectContainer('forestZone');
            $manager->createObjectContainer('forestInput');
            $manager->createObjectContainer('forestOutput');
            $manager->createObjectContainer('forestImport');
            $manager->createObjectContainer('forestImportSource');
            $manager->createObjectContainer('forestWriting');

            break;
    }
}

return true;