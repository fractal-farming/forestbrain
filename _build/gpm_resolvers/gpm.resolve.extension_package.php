<?php
/**
 * Resolve creating db tables
 *
 * THIS RESOLVER IS AUTOMATICALLY GENERATED, NO CHANGES WILL APPLY
 *
 * @package forestbrain
 * @subpackage build
 *
 * @var mixed $object
 * @var modX $modx
 * @var array $options
 */

if ($object->xpdo) {
    $modx =& $object->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            $modelPath = $modx->getOption('forestbrain.core_path');

            if (empty($modelPath)) {
                $modelPath = '[[++core_path]]components/forestbrain/model/';
            }

            if ($modx instanceof modX) {
                $modx->addExtensionPackage('forestbrain', $modelPath, array (
  'serviceName' => 'forestbrain',
  'serviceClass' => 'ForestBrain',
));
            }

            break;
        case xPDOTransport::ACTION_UNINSTALL:
            if ($modx instanceof modX) {
                $modx->removeExtensionPackage('forestbrain');
            }

            break;
    }
}

return true;