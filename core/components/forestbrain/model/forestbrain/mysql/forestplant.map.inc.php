<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestPlant']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'extends' => 'earthPlant',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'fieldAliases' => 
  array (
    'resource_id' => 'parent_id',
    'forest_id' => 'parent_id',
  ),
  'composites' => 
  array (
    'Images' => 
    array (
      'class' => 'forestImagePlant',
      'local' => 'id',
      'foreign' => 'parent_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'Notes' => 
    array (
      'class' => 'forestNotePlant',
      'local' => 'id',
      'foreign' => 'parent_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
  'aggregates' => 
  array (
    'Forest' => 
    array (
      'class' => 'forestData',
      'local' => 'parent_id',
      'foreign' => 'resource_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Resource' => 
    array (
      'class' => 'modResource',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
