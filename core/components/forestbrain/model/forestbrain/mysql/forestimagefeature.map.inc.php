<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestImageFeature']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'extends' => 'earthImage',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Forest' => 
    array (
      'class' => 'forestData',
      'local' => 'parent_id',
      'foreign' => 'forest_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
