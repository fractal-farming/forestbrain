<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestSource']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'extends' => 'earthSource',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'fieldAliases' => 
  array (
    'resource_id' => 'parent_id',
    'forest_id' => 'parent_id',
  ),
  'composites' => 
  array (
    'Imports' => 
    array (
      'class' => 'forestImportSource',
      'local' => 'id',
      'foreign' => 'source_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
  'aggregates' => 
  array (
    'Forest' => 
    array (
      'class' => 'forestData',
      'local' => 'parent_id',
      'foreign' => 'resource_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Resource' => 
    array (
      'class' => 'modResource',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
