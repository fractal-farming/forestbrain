<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestExchangeItemResource']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'extends' => 'earthExchangeItem',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'fieldAliases' => 
  array (
    'import_id' => 'parent_id',
  ),
  'aggregates' => 
  array (
    'Import' => 
    array (
      'class' => 'forestImport',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
