<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestNeedComp']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'table' => 'forestbrain_need_comp',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'need_id' => 0,
    'component_id' => 0,
  ),
  'fieldMeta' => 
  array (
    'need_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'component_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'indexes' => 
  array (
    'need_comp' => 
    array (
      'alias' => 'need_comp',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'need_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'component_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Need' => 
    array (
      'class' => 'forestNeed',
      'local' => 'need_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Component' => 
    array (
      'class' => 'forestComponent',
      'local' => 'component_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
