<?php
/**
 * @package ForestBrain
 */
$xpdo_meta_map['forestNotePlant']= array (
  'package' => 'forestbrain',
  'version' => '1.1',
  'extends' => 'earthNote',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
  ),
  'fieldMeta' => 
  array (
  ),
  'aggregates' => 
  array (
    'Plant' => 
    array (
      'class' => 'forestPlant',
      'local' => 'parent_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
