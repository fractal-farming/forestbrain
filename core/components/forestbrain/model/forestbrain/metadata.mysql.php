<?php

$xpdo_meta_map = array (
  'xPDOSimpleObject' => 
  array (
    0 => 'forestData',
    1 => 'forestFeature',
    2 => 'forestNeed',
    3 => 'forestComponent',
    4 => 'forestNeedComp',
    5 => 'forestZone',
    6 => 'forestInput',
    7 => 'forestOutput',
    8 => 'forestImport',
    9 => 'forestImportSource',
    10 => 'forestWriting',
  ),
  'earthPlant' => 
  array (
    0 => 'forestPlant',
    1 => 'forestPlantNeed',
  ),
  'earthSource' => 
  array (
    0 => 'forestSource',
  ),
  'earthExchangeItem' => 
  array (
    0 => 'forestExchangeItemResource',
  ),
  'earthImage' => 
  array (
    0 => 'forestImage',
    1 => 'forestImagePlant',
    2 => 'forestImageFeature',
    3 => 'forestImageComponent',
    4 => 'forestImageWriting',
  ),
  'earthNote' => 
  array (
    0 => 'forestNotePlant',
  ),
);