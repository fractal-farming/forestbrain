{
    "formtabs": [
        {
            "MIGX_id": 1,
            "caption": "Need \/ Want",
            "print_before_tabs": "0",
            "pos": "",
            "fields": [
                {
                    "MIGX_id": "",
                    "field": "id",
                    "caption": "ID",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "component_id",
                    "caption": "Component ID",
                    "description": "This points to the component resource.",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "number",
                    "validation": "",
                    "configs": "",
                    "restrictive_condition": "",
                    "display": "none",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                },
                {
                    "MIGX_id": "",
                    "field": "need_id",
                    "caption": "Why is it needed?",
                    "description": "",
                    "description_is_code": "0",
                    "inputTV": "",
                    "inputTVtype": "listbox",
                    "validation": "",
                    "configs": {
                        "typeAhead": "false"
                    },
                    "restrictive_condition": "",
                    "display": "",
                    "sourceFrom": "config",
                    "sources": "",
                    "inputOptionValues": "@CHUNK tvSelectForestNeed",
                    "default": "",
                    "useDefaultIfEmpty": "0",
                    "pos": ""
                }
            ]
        }
    ],
    "contextmenus": "update||duplicate||remove",
    "actionbuttons": "addItem",
    "columnbuttons": "",
    "extended": {
        "migx_add": "Link an existing need",
        "disable_add_item": "",
        "add_items_directly": "",
        "formcaption": "",
        "update_win_title": "Link need",
        "win_id": "need_comp",
        "maxRecords": "",
        "addNewItemAt": "top",
        "media_source_id": "",
        "multiple_formtabs": "",
        "multiple_formtabs_label": "",
        "multiple_formtabs_field": "",
        "multiple_formtabs_optionstext": "",
        "multiple_formtabs_optionsvalue": "",
        "actionbuttonsperrow": 4,
        "winbuttonslist": "",
        "extrahandlers": "this.handleColumnSwitch",
        "filtersperrow": 4,
        "packageName": "forestbrain",
        "classname": "forestNeedComp",
        "task": "",
        "getlistsort": "id",
        "getlistsortdir": "DESC",
        "sortconfig": "",
        "gridpagesize": 20,
        "use_custom_prefix": "0",
        "prefix": "",
        "grid": "",
        "gridload_mode": 2,
        "check_resid": 0,
        "check_resid_TV": "",
        "join_alias": "",
        "has_jointable": "yes",
        "getlistwhere": {
            "component_id": "[[+object_id]]"
        },
        "joins": [
            {
                "alias": "Component"
            },
            {
                "alias": "Need"
            }
        ],
        "hooksnippets": {
            "aftersave": "migxSaveNeedComp"
        },
        "cmpmaincaption": "",
        "cmptabcaption": "",
        "cmptabdescription": "",
        "cmptabcontroller": "",
        "winbuttons": "",
        "onsubmitsuccess": "",
        "submitparams": ""
    },
    "columns": [
        {
            "MIGX_id": "",
            "header": "ID",
            "dataIndex": "id",
            "width": 10,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Component",
            "dataIndex": "Component_resource_id",
            "width": 30,
            "sortable": true,
            "show_in_grid": 0,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+Component_resource_pagetitle]] ([[+Component_resource_id]])",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Need",
            "dataIndex": "Need_title",
            "width": 80,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "",
            "renderoptions": "",
            "editor": ""
        },
        {
            "MIGX_id": "",
            "header": "Priority",
            "dataIndex": "Need_priority",
            "width": 20,
            "sortable": true,
            "show_in_grid": 1,
            "customrenderer": "",
            "renderer": "this.renderChunk",
            "clickaction": "",
            "selectorconfig": "",
            "renderchunktpl": "[[+Need_priority:eq=`3`:then=`Need`]][[+Need_priority:eq=`2`:then=`Want`]][[+Need_priority:lt=`2`:then=`Wish`]]",
            "renderoptions": "",
            "editor": ""
        }
    ],
    "category": ""
}