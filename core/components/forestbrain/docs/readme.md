# What is ForestBrain?

ForestBrain is a tool to collect and share forest data. This data can then be placed into a bigger picture by its members. By working together, we can try to figure out: what do forests need in order to thrive? How can we best support the forests? And are our efforts actually working?

## Backend

This software is distributed as an extra for MODX, a content management framework based on PHP and MySQL. Although the database model could in theory be used in other MySQL-based applications (such as Directus), for now it relies on MODX for providing the interface parts.

The following extras are required. They will be added automatically when installing ForestBrain in MODX:

- EarthBrain
- Guzzle7
- pdoTools
- MIGX
- ImagePlus
- pThumb
- SuperBoxSelect

The following paid extras are optional, but recommended:

- Agenda (for managing food related events)
- ContentBlocks (for creating responsive web pages)
- Redactor (the rich text editor of choice)

## Frontend

A front-end framework called Fomantic UI is used for presenting ForestBrain data in a browser. You can of course choose to roll out your own HTML/CSS, but with Fomanctic UI this will be plug and (dis)play.

If it's not your intention to display your data on a website, then you don't need to worry about any HTML/CSS. The interface for managing data is provided by MODX and MIGX.

## Romanesco

If you _are_ planning to use the data on a website, then you might want to take a look at Romanesco. Romanesco is a collection of tools for prototyping and building websites. It integrates a front-end pattern library (using Semantic UI) directly into the CMS (MODX).

So if you install Romanesco, everything you need to use ForestBrain is already included. Romanesco is also open source and free to download and use.

See https://romanesco.info for more information.

## License

ForestBrain is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

[GNU General Public License v3](https://gitlab.com/fractal-farming/forestbrain/blob/master/core/components/forestbrain/docs/license.md)