# Changelog for ForestBrain

## ForestBrain 0.6.3
Released on January 25, 2025

Fixes and improvements:
- Fix transport package installation (autoloader is not yet available)
- Attach extended Images and Notes to forestPlant object
- Properly use collection graph to load joined Input objects
- Provide path manually for map popup image
- Tweak getPlants function
- Remove generateImagePath function
- Add zoom and KML fields to Location grids
- Alter CB map field CSS based on container size
- Fix source paths for images in map popups
- Autoload ForestBrain namespace and base class
- Forward additional MIGX properties to EarthBrain class
- Load ForestBrain as extension package
- Tighten permissions for all MIGX grids

## ForestBrain 0.6.2
Released on August 6, 2024

New features:
- Add geocoding options to Forests and Components

Fixes and improvements:
- Segment custom cache based on user access level
- Fall back on EarthBrain box types if no cases are hit
- Add elements for displaying Components on map
- Use lexicon strings in MIGX configs
- Add extended ExchangeItem object for resource exchanges

## ForestBrain 0.6.1
Released on October 8, 2023

New features:
- Add database table and grid for Writings
- Add MIGX TV for maintaining a plant species wishlist
- Add MIGX TVs for displaying Nursery items and Outputs (read-only)
- Add search field to Component, Feature and Plant grids
- Add basic templating engine for MIGX configs
- Add extended Plant object for wishlist entries

Fixes and improvements:
- Allow Component content to be static Markdown 
- Allow Components to be stored under alternative parent
- Fix images in Component and Feature grids
- Disable saving person when saving a forest
- Use new cover images in forest overviews
- Add ability to select resource images in non-resource grids
- Connect forest images directly to resource
- Remove individual image variants and use EarthBrain images instead
- Reshuffle forest TV tabs and TVs
- Remove inline styles from floated grid icons (done with CSS now)
- Hide redundant title column when grid is loaded inside forest resource

## ForestBrain 0.6.0
Released on May 24, 2023

First release after corpus calloscotomy, or split brain surgery. FoodBrain has been
divided into separate halves: EarthBrain and ForestBrain. EarthBrain contains
the basic, most generic types of data, which can be used (and extended) by
ForestBrain and other packages.

This repository continues as ForestBrain. The following database tables have
been migrated to EarthBrain:

- Species (now Taxon)
- Seed
- Plant
- PlantItem (now Planting)
- User (now Person)
- Source
- Address
- Location

## FoodBrain 0.6.0-beta1
Released on March 9, 2022

New features:
- Add snippet to import data from Kobo [WIP]
- Add template for members pages [WIP]
- Add form for changing user password [WIP]
- Add login button for main menu [WIP]

Fixes and improvements:
- Fix form dropdown for selecting forest
- Set extended information for activated FoodUser

## FoodBrain 0.5.2
Released on January 22, 2022

New features:
- Add grid for editing contacts

Fixes and improvements:
- Don't create address or location if there's no data
- Replace hardcoded reference to map CB with system setting
- Add middle name to contact data

## FoodBrain 0.5.1
Released on December 10, 2021

New features:
- Add component table overview
- Add component overviews

Fixes and improvements:
- Control map base layers and default layers with CB setting
- Add search to Taxonomy grid
- Allow inputs and outputs to be connected to plants
- Don't display requirements from other forests in forest resource
- Move components grid to forest resource
- Tie component relations to object instead of resource
- Refer to maintainers as Stewards

## FoodBrain 0.5.0
Released on October 25, 2021

New features:
- Add forest table overview
- Add local forest template to improve sharing data across contexts
- Add ability to obfuscate coordinates
- Create separate database table for seeds
- Add forest features
- Add forest zones
- Dynamically load multiple map layers with a repeater CB
- Add forest components
- Add ForestBrain menu

Fixes and improvements:
- Compatibility fixes for Romanesco 1.0.0-beta8
- Add dropdown row tpl for selecting plant species in form
- Add resource field to edit maintainer (createdby)
- Add tab for managing species to 'Edit plant' modal
- Add variety field to plants
- Prevent save action in forest CMP on error
- Delete foodUser data if user is deleted
- Rename migxProcessDecimals to migxVerifyData
- Don't generate new user when changing a contact
- Update createdby values of related data when changing forest maintainer
- Activate contact information in forest properties
- Don't load individual plants grid when adding a new plant
- Add snippet to load forest boundaries layer on map
- Fix fatal error in forest map when plant item has no name
- Add terrain / satellite switch to map
- Move address data from locations to separate table

## FoodBrain 0.4.1
Released on July 20, 2020

New features:
- Add event detail template

Fixes and improvements:
- Display plant list with labels in forest template
- Add StuartXchange URL to taxonomy table
- Add resolver for syncing db tables after package update
- Improve layout of forest template

## FoodBrain 0.4.0
Released on May 4, 2020

New features:
- Add forest overviews
- Add image to food map popup
- Add food map toolbar

Fixes and improvements:
- Make container ID dynamic in forest map
- Add custom template elements for events
- Automatically arrange MIGX images in subfolders, by resource ID
- Move cover images to foodForest table

## FoodBrain 0.3.2
Released on January 1, 2020

Fixes and improvements:
- Only activate mousewheel zoom on focus in map container
- Show plant species in map popup
- Show buttons with website and facebook links in map popup
- Disable plant items and events on food forest map
- Add social media channels and website to foodForest table

## FoodBrain 0.3.1
Released on December 26, 2019

New features:
- Add LGU field to Location table

## FoodBrain 0.3.0
Released on December 25, 2019

New features:
- Add map view with forests, plant items and events
- Use Mapbox for loading tiles and styles
- Use Agenda extra for managing events

Fixes and improvements:
- Fix incorrect reference to forest resource ID in schema
- Don't load forests without coordinates in map
- Change location field for longitude to lng
- Add editedon and editedby fields to user-editable tables
- Don't rely on post values for retrieving data from MIGXdb objects
- Improve display of data in MIGX grids

## FoodBrain 0.2.0
Released on December 1, 2019

New features:
- Add foodUser and foodUserData objects
- Add Gitify configs for extracting dev and live data

Fixes and improvements
- Rename foodTaxonomy class to foodSpecies
- Remove MIGX_id and position values from configs
- Add species lifecycle to taxonomy properties
- Fix contact data not being saved when adding new source
- Add common snippet functions to FoodBrain class
- Improvements to foodSource config and saveContact function
- Add elevation field to Locations
- Integrate Location settings in Forest config
- Create hook snippets per object for MIGXdb configs

## FoodBrain 0.1.0
Released on November 7, 2019

Initial project setup:
- Add MIGXdb configs
- Add database schema
