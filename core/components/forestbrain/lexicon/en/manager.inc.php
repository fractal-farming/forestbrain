<?php

// Collections
// =====================================================================

$_lang['collections.forest.children'] = 'Forests';
$_lang['collections.forest.children.create'] = 'Add Forest';
$_lang['collections.forest.children.back_to_collection_label'] = 'All Forests';

$_lang['collections.component.children'] = 'Components';
$_lang['collections.component.children.create'] = 'Add Component';
$_lang['collections.component.children.back_to_collection_label'] = 'All Components';

$_lang['collections.writing.children'] = 'Writings';
$_lang['collections.writing.children.create'] = 'Add Writing';
$_lang['collections.writing.children.back_to_collection_label'] = 'All Writings';
