<?php

// Map
// =====================================================================

$_lang['forestbrain.map.button_read_more'] = "More info";
$_lang['forestbrain.map.location_obfuscated_heading'] = "This is not the exact location.";
$_lang['forestbrain.map.location_obfuscated_content'] = "For privacy / security reasons, the marker has been placed randomly within a [[+radius]] km radius.";

// Forest
// =====================================================================

$_lang['forestbrain.forest.heading'] = "Forest";
$_lang['forestbrain.forest.trees'] = "Trees";
$_lang['forestbrain.forest.person'] = "Guardian";
$_lang['forestbrain.forest.admin'] = "Steward";

$_lang['forestbrain.forest.data_heading'] = "Forest details";
$_lang['forestbrain.forest.data_category'] = "Category";
$_lang['forestbrain.forest.data_size'] = "Land area";
$_lang['forestbrain.forest.data_age'] = "Age";
$_lang['forestbrain.forest.data_access'] = "Access";
$_lang['forestbrain.forest.data_website'] = "Website";
$_lang['forestbrain.forest.data_facebook'] = "Facebook page";

// Location
// =====================================================================

$_lang['forestbrain.location.heading'] = "Location";
$_lang['forestbrain.location.elevation'] = "Elevation";

// Source
// =====================================================================

$_lang['forestbrain.source.heading'] = "Source";
$_lang['forestbrain.source.unknown'] = "Unknown / Nature";

// Address
// =====================================================================

$_lang['forestbrain.address.heading'] = "Address";

// Component
// =====================================================================

$_lang['forestbrain.component.heading'] = "Component";
$_lang['forestbrain.component.admin'] = "Admin";

$_lang['forestbrain.component.data_zone'] = "Zone";
$_lang['forestbrain.component.data_type'] = "Type";
$_lang['forestbrain.component.data_stage'] = "Stage";

$_lang['forestbrain.component.inputs_heading'] = "Inputs";
$_lang['forestbrain.component.input_heading'] = "Input";
$_lang['forestbrain.component.input_name'] = "Name";
$_lang['forestbrain.component.input_type'] = "Type";
$_lang['forestbrain.component.input_internal'] = "Output of another component";
$_lang['forestbrain.component.input_external'] = "External source";

$_lang['forestbrain.component.outputs_heading'] = "Outputs";
$_lang['forestbrain.component.output_heading'] = "Output";
$_lang['forestbrain.component.output_name'] = "Name";
$_lang['forestbrain.component.output_type'] = "Type";
$_lang['forestbrain.component.output_resilience'] = "Resilience";
$_lang['forestbrain.component.output_quality'] = "Quality";
$_lang['forestbrain.component.output_availability'] = "Availability";
