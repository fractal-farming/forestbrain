<?php
/**
 * @package forestbrain
 */

namespace FractalFarming\ForestBrain;

use modX;
use Seld\JsonLint\JsonParser;

class ForestBrain
{
    /**
     * A reference to the modX instance
     * @var modX $modx
     */
    public modX $modx;

    /**
     * The namespace
     * @var string $namespace
     */
    public string $namespace = 'forestbrain';

    /**
     * A configuration array
     * @var array $config
     */
    public array $config = [];

    /**
     * ForestBrain constructor
     *
     * @param modX $modx A reference to the modX instance.
     * @param array $config An array of configuration options. Optional.
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('forestbrain.core_path', $config, $this->modx->getOption('core_path') . 'components/forestbrain/');
        $assetsUrl = $this->modx->getOption('forestbrain.assets_url', $config, $this->modx->getOption('assets_url') . 'components/forestbrain/');

        $this->config = array_merge([
            'basePath' => $this->modx->getOption('base_path'),
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
        ], $config);

        $corePath = $this->modx->getOption('earthbrain.core_path', $config, $this->modx->getOption('core_path') . 'components/earthbrain/');
        $earthbrain = $this->modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

        // Load EarthBrain classes that will be extended first
        $this->modx->loadClass('earthPlant', $corePath . 'model/earthbrain/');
        $this->modx->loadClass('earthImage', $corePath . 'model/earthbrain/');

        $this->modx->loadClass('forestData', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestPlant', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestFeature', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestNeed', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestComponent', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestNeedComp', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestZone', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestInput', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestOutput', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestImport', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestImportSource', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestImage', $this->config['modelPath'] . 'forestbrain/');
        $this->modx->loadClass('forestWriting', $this->config['modelPath'] . 'forestbrain/');

        $this->modx->addPackage('forestbrain', $this->config['modelPath']);

        // Additional MIGX properties for EarthBrain class
        $earthbrain->addMigxProperties([
            'forestbrain_plants:forestbrain' => [
                'category' => 'plant',
                'symCategory' => 'forest', // symlinks will be created here
                'className' => 'forestPlant',
                'classKeys' => [
                    'image' => 'forestImagePlant',
                    'note' => 'forestNotePlant',
                ],
                'fieldName' => 'parent_id',
            ],
            'forestbrain_features:forestbrain' => [
                'category' => 'feature',
                'symCategory' => 'forest',
                'className' => 'forestFeature',
                'classKeys' => [
                    'image' => 'forestImageFeature',
                ],
                'fieldName' => 'forest_id',
            ],
            'forestbrain_components:forestbrain' => [
                'category' => 'component',
                'symCategory' => 'forest',
                'className' => 'forestComponent',
                'classKeys' => [
                    'image' => 'forestImageComponent',
                ],
                'fieldName' => 'forest_id',
            ],
            'forestbrain_writings:forestbrain' => [
                'category' => 'writing',
                'symCategory' => 'forest',
                'className' => 'forestWriting',
                'classKeys' => [
                    'image' => 'forestImageWriting',
                ],
                'fieldName' => 'forest_id',
            ]
        ]);
    }

    /**
     * Find all plants attached to given forest.
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @param int $limit
     * @return string
     */
    public function getPlants(array $properties, string $rowTpl = '', string $divider = ', ', int $limit = 0): string
    {
        // Use global ID if present
        $forestID = $properties['forest_global_id'] ?? $properties['id'];

        $result = [];
        $plants = $this->modx->getCollectionGraph('forestPlant', '{"Taxon":{}}', [
            'forest_id' => $forestID,
        ]);

        $idx = 0;
        foreach ($plants as $plant) {
            if ($limit && $idx++ >= $limit) break;
            if ($plant->get('taxon_id')) {
                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, [
                        'Taxon_name' => $plant->Taxon->get('name'),
                        'Taxon_name_local' => $plant->Taxon->get('name_local'),
                        'Taxon_name_latin' => $plant->Taxon->get('name_latin'),
                        'variety' => $plant->get('variety'),
                    ]);
                } else {
                    $result[] = $plant->Taxon->get('name');
                }
            }
        }

        return implode($divider, $result);
    }

    /**
     * Find all inputs attached to given component.
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @return string
     */
    public function getInputs(array $properties, string $rowTpl = '', string $divider = ', '): string
    {
        $result = [];
        $inputs = $this->modx->getCollectionGraph('forestInput', '{"Output":{},"Import":{}}', [
            'component_id' => $properties['component_id'],
        ]);

        foreach ($inputs as $input) {
            if ($input->get('output_id')) {
                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, [
                        'Output_name' => $input->Output->get('name'),
                        'Output_type' => $input->Output->get('type'),
                        'Output_resilience' => $input->Output->get('resilience'),
                        'Output_quality' => $input->Output->get('quality'),
                        'Output_availability' => $input->Output->get('availability'),
                        'type' => $input->get('type'),
                    ]);
                } else {
                    $result[] = $input->Output->get('name');
                }
            }
            if ($input->get('import_id')) {
                if ($rowTpl) {
                    $result[] = $this->modx->getChunk($rowTpl, [
                        'Import_name' => $input->Import->get('name'),
                        'Import_resilience' => $input->Import->get('resilience'),
                        'type' => $input->get('type'),
                    ]);
                } else {
                    $result[] = $input->Import->get('name');
                }
            }
        }

        return implode($divider, $result);
    }

    /**
     * Find all outputs attached to given component.
     *
     * @param array $properties
     * @param string $rowTpl
     * @param string $divider
     * @return string
     */
    public function getOutputs(array $properties, string $rowTpl = '', string $divider = ', '): string
    {
        $result = [];
        $outputs = $this->modx->getCollection('forestOutput', [
            'component_id' => $properties['component_id'],
        ]);

        foreach ($outputs as $output) {
            $result[] = $output->get('name');
        }

        return implode($divider, $result);
    }
}