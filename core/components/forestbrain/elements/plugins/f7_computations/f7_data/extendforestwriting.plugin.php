<?php
/**
 * ExtendForestWriting
 *
 * Generate row for writing resource in the forestbrain_writings table.
 *
 * IMPORTANT! This requires a context setting that links to the forest ID:
 * forestbrain.forest_global_id
 *
 * @var modX $modx
 */

switch ($modx->event->name) {
    case 'OnDocFormSave':
        /**
         * @var modResource $resource
         * @var int $id
         */

        // Abort if template is not ForestComponent
        $templateID = $resource->get('template');
        if ($templateID != $modx->getOption('forestbrain.writing_template_id')) {
            break;
        }

        // Abort if resource is created in MIGX (meaning: object already exists)
        if ($resource->get('longtitle') == 'It is I') {
            $resource->set('longtitle', '');
            $resource->save();
            break;
        }

        // Get global forest page from context settings
        $forestSetting = $modx->getObject('modContextSetting', [
            'context_key' => $resource->get('context_key'),
            'key' => 'forestbrain.forest_global_id'
        ]);

        if (!is_object($forestSetting)) break;

        // Check if writing already exists in table
        $writing = $modx->getObject('forestWriting', [
            'resource_id' => $id,
        ]);

        // Create new object or update existing
        if (!is_object($writing)) {
            $writing = $modx->newObject('forestWriting', [
                'resource_id' => $id,
                'forest_id' => $forestSetting->get('value'),
                'createdon' => time(),
                'createdby' => $modx->user->get('id'),
            ]);
        } else {
            $writing->set('forest_id', $forestSetting->get('value'));
        }

        $writing->save();

        break;
}

return true;