<?php
/**
 * ExtendForestComponent
 *
 * Generate row for component resource in the forestbrain_components table.
 *
 * IMPORTANT! This requires a context setting that links to the forest ID:
 * forestbrain.forest_global_id
 *
 * @var modX $modx
 */

switch ($modx->event->name) {
    case 'OnDocFormSave':
        /**
         * @var modResource $resource
         * @var int $id
         */

        // Abort if template is not ForestComponent
        $templateID = $resource->get('template');
        if ($templateID != $modx->getOption('forestbrain.component_template_id')) {
            break;
        }

        // Abort if resource is created in MIGX (meaning: object already exists)
        if ($resource->get('longtitle') == 'It is I') {
            $resource->set('longtitle', '');
            $resource->save();
            break;
        }

        // Get global forest page from context settings
        $forestSetting = $modx->getObject('modContextSetting', [
            'context_key' => $resource->get('context_key'),
            'key' => 'forestbrain.forest_global_id'
        ]);

        if (!is_object($forestSetting)) break;

        // Check if component already exists in table
        $component = $modx->getObject('forestComponent', [
            'resource_id' => $id,
        ]);

        // Create new component or update existing
        if (!is_object($component)) {
            $component = $modx->newObject('forestComponent', [
                'resource_id' => $id,
                'forest_id' => $forestSetting->get('value'),
                'createdon' => time(),
                'createdby' => $modx->user->get('id'),
            ]);
        } else {
            $component->set('forest_id', $forestSetting->get('value'));
        }

        $component->save();

        break;
}

return true;