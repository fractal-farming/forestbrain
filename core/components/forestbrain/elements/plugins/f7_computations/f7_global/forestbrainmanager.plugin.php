<?php
/**
 * ForestBrainManager
 *
 * Small tweaks to the MODX backend, to enhance the ForestBrain experience.
 *
 * @var modX $modx
 * @var modManagerController $controller
 *
 * @package forestbrain
 */

$modx->controller->addLexiconTopic('forestbrain:manager');

switch ($modx->event->name) {
    case 'OnManagerPageAfterRender':
        $managerContent = $modx->controller->content;

        // Remove prefix from TV tabs
        $managerContent = str_replace('class="x-tab" title="Forest - ', 'class="x-tab" title="', $managerContent);

        $controller->content = $managerContent;
        break;
}

return;