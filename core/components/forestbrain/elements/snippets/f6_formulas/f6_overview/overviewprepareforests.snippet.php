<?php
/**
 * overviewPrepareForests
 *
 * Modify field values before the pdoResources result is rendered.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

$tplPlantRow = $modx->getOption('tplPlantRow', $scriptProperties, 'earthTaxonBasic');
$tplImageRow = $modx->getOption('tplImageRow', $scriptProperties, 'imgOverviewJSON');

//$modx->log(modX::LOG_LEVEL_ERROR, '[overviewPrepareForests] Row: ' . print_r($row, 1));
//$row = array();

// Links
// =============================================================================

// Create buttons
$row['button_website'] = '';
$row['button_facebook'] = '';
if ($row['website'] ?? false) {
    $row['button_website'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['website'],
        'icon_class' => 'globe icon',
    ));
}
if ($row['facebook'] ?? false) {
    $row['button_facebook'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['facebook'],
        'icon_class' => 'facebook f icon',
    ));
}

// Generate links
$row['link'] = $modx->makeUrl($row['id']);


// Images
// =============================================================================

if ($row['img_type']) {
    $img = $row['img_' . $row['img_type']] ?? null;
    if ($img) {
        $img['class_key'] = 'forestImage';
        $img['parent_id'] = $row['forest_global_id'];
        $row['image_json'] = $earthimage->fixSourcePath($img);
    }
}

// Forest data
// =============================================================================

// Get plant species
$row['plants'] = $forestbrain->getPlants($row,$tplPlantRow,'<br>');

// Get category name
if ($row['category'] ?? false) {
    $rmOption = $modx->getObject('rmOption', array(
        'key' => 'forest_category',
        'alias' => $row['category'],
    ));
    if ($rmOption) {
        $row['category'] = $rmOption->get('name');
    }
}


return json_encode($row);