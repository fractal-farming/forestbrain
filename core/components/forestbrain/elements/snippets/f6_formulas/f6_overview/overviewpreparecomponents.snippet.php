<?php
/**
 * overviewPrepareComponents
 *
 * Modify field values before the pdoResources snippet in
 * overviewOuterComponents is executed.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;
if (!($romanesco instanceof Romanesco)) return;

//$modx->log(modX::LOG_LEVEL_ERROR, '[overviewPrepareForests] Row: ' . print_r($row, 1));
//$row = array();


// Data
// =============================================================================

$row['inputs'] = $forestbrain->getInputs($row, '', '<br>');
$row['outputs'] = $forestbrain->getOutputs($row, '', '<br>');


// Images
// =============================================================================

if ($row['image'] ?? false) {
    // Generate correct image path
    $image = $earthimage->setImagePath($row);

    $row['image_json'] = json_encode($image);
}
else {
    // Provide fallback
    $row['image_fallback'] = '/uploads/img/fallback/fallback_' . $row['img_type'] . '.png';
}

return json_encode($row);