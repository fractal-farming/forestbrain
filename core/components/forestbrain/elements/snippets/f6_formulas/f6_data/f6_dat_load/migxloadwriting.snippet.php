<?php
/**
 * migxLoadWriting
 *
 * Forward resource ID, to enable selecting images in stand-alone MIGX grids.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$forestID = $scriptProperties['record']['forest_id'] ?? '';
if ($forestID) {
    $_POST['forest_id'] = $forestID;
}

return '';