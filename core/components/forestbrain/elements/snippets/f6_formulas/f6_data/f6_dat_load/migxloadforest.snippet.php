<?php
/**
 * migxLoadForest
 *
 * Forward resource ID, to enable selecting images in stand-alone MIGX grids.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$resourceID = $scriptProperties['record']['resource_id'] ?? '';
if ($resourceID) {
    $_POST['resource_id'] = $resourceID;
}

return '';