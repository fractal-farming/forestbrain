<?php
/**
 * migxSaveInput
 *
 * Aftersave snippet for inputs. Inputs can be tied to a component or a plant.
 *
 * The MIGX config contains a key with the win_id of the parent object,
 * by which the parent class can be determined. The id forwarded by co_id
 * doesn't provide that information.
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);

//$modx->log(modX::LOG_LEVEL_ERROR, print_r($_REQUEST,1));
//$modx->log(modX::LOG_LEVEL_ERROR, print_r($properties,1));

switch ($properties['parent']) {
    case 'component':
        $object->set('component_id', $properties['co_id']);
        break;
    case 'plant':
        $object->set('plant_id', $properties['co_id']);
        break;
}

$object->save();

$earthbrain->resetNull($object, $properties);

return '';