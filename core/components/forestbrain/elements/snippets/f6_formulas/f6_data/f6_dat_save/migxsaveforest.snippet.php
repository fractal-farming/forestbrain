<?php
/**
 * migxSaveForest
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthlocation = $modx->getService('earthlocation','earthLocation',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/',array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthlocation instanceof earthLocation)) return;
if (!($earthimage instanceof earthImage)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);
$result = [];

if (is_object($object))
{
    $earthbrain->resetNull($object, $properties);

    // Geocode addresses
    if ($properties['Location_geocode']) {
        $location = $earthlocation->geocodeAddress($properties['Location_geocode']);

        // Abort on error
        if (!$location || $location['error']) {
            return json_encode($location);
        }

        // Update coordinates and address, or just the coordinates
        if ($properties['Location_update_address']) {
            $properties = array_merge($properties, $location);
        } else {
            $properties['Location_lat'] = $location['lat'];
            $properties['Location_lng'] = $location['lng'];
        }
    }
    elseif ($properties['Location_geocode_reverse']) {
        $location = $earthlocation->geocodeAddressReverse($properties['Location_geocode_reverse']);
        $properties = array_merge($properties, $location);
    }

    // Attempt to extract location from image
    if ($properties['Location_from_image']) {
        $path = $properties['Location_from_image'];
        $source = $modx->getOption('earthbrain.img_source_meta');

        if ($location = $earthimage->getExifData($path, $source)) {
            $properties['Location_lat'] = $location['lat'];
            $properties['Location_lng'] = $location['lng'];
            $properties['Location_elevation'] = $location['elevation'];
        }
    }

    $earthbrain->saveLocation($object, $properties);
    $earthbrain->saveAddress($object, $properties);

    // Update createdby values of related data when changing forest admin
    if ($properties['createdby'] != $properties['Resource_createdby']) {
        //$object->set('Resource_createdby', $properties['createdby']);
        //$object->set('Address_createdby', $properties['createdby']);
        //$object->set('Location_createdby', $properties['createdby']);

        // NB: setting a value on a joint field (like above) doesn't seem to work.
        $resource = $object->getOne('Resource');
        $resource->set('createdby', $properties['createdby']);
        $resource->save();

        $address = $object->getOne('Address');
        $address->set('createdby', $properties['createdby']);
        $address->save();

        $location = $object->getOne('Location');
        $location->set('createdby', $properties['createdby']);
        $location->save();

        // Also transfer ownership of connected plants
        $plants = $object->getMany('Plants');
        foreach ($plants as $plant) {
            $plant->set('createdby', $properties['createdby']);
            $plant->save();

            // Individual items
            $plantItems = $plant->getMany('Plantings');
            foreach ($plantItems as $item) {
                $plant->set('createdby', $properties['createdby']);
                $plant->save();
            }
        }
    }
}

return json_encode($result);