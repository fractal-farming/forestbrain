<?php
/**
 * migxSaveWriting
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);

if (!is_object($object)) return;

$result = [];
$resourceID = $properties['resource_id']; // Parent resource!!
$locationID = $object->get('location_id');
$uriOverride = 0;
$uri = '';

// Find ID of writing_container_id TV
$query = $modx->newQuery('modTemplateVar', ['name' => 'writing_container_id']);
$query->select('id');
$tmplVarID = $modx->getValue($query->prepare());

// Find alternative parent
$query = $modx->newQuery('modTemplateVarResource', [
    'contentid' => $resourceID,
    'tmplvarid' => $tmplVarID,
]);
$query->select('value');
$parentID = $modx->getValue($query->prepare());
if (!$parentID && $resourceID) {
    $parentID = $resourceID;
    $uriOverride = 1;
}

// Define context
$contextKey = $modx->getOption('forestbrain.writing_context_key', $scriptProperties, 'web');
if ($parentID && $parentID != $resourceID) {
    $query = $modx->newQuery('modResource', ['id' => $parentID]);
    $query->select('context_key');
    $contextKey = $modx->getValue($query->prepare());
}

// Default settings for regular resources
$templateID = $modx->getOption('forestbrain.writing_template_id', $scriptProperties);
$classKey = $modx->getOption('forestbrain.writing_class_key', $scriptProperties, 'modDocument');
$richText = 1;
$contentType = 'text/html'; // deprecated in MODX3
$content_type = 1;

// Adjustments for static Markdown resources
if ($classKey == 'modStaticResource') {
    $richText = 0;
    $contentType = 'text/x-markdown';
    $content_type = 11;
}

$resourceProperties = [
    'pagetitle' => $postValues['Resource_pagetitle'] ?? '',
    'description' => $postValues['Resource_description'] ?? '',
    'introtext' => $postValues['Resource_introtext'] ?? '',
    'alias' => $postValues['Resource_alias'] ?? '',
    'content' => $postValues['Resource_content'] ?? '',
    'richtext' => $richText,
    'published' => $postValues['Resource_published'] ?? '',
    'parent' => $parentID,
    'template' => $templateID,
    'searchable' => 1,
    'hidemenu' => 1,
    'show_in_tree' => 0,
    'class_key' => $classKey,
    'context_key' => $contextKey,
    'content_type' => $content_type,
    'contentType' => $contentType,
    'uri_override' => $uriOverride,
];

// Create component resource if MIGX object is new
if ($properties['object_id'] == 'new')
{
    // Let ExtendWritingComponent plugin know that the object already exists
    $resourceProperties['longtitle'] = 'It is I';

    // Run, Forest!
    $response = $modx->runProcessor('resource/create', $resourceProperties);

    if (!$response->isError()) {
        // Resource object only returns array with ID
        $resource = $response->getObject();

        if ($resource['id']) {
            // Add virtual sub folder to URI
            if ($uriOverride) {
                $resourceObject = $modx->getObject('modResource', $resource['id']);
                $resourceAlias = $resourceObject->get('alias');
                $resourceURI = $resourceObject->get('uri');
                $resourceURI = str_replace($resourceAlias, 'components/' . $resourceAlias, $resourceURI);

                $resourceObject->set('uri', $resourceURI);
                $resourceObject->set('uri_override', 1);
                $resourceObject->save();
            }

            // Reference resource in object
            $object->set('resource_id', $resource['id']);
        }

        $modx->log(MODX::LOG_LEVEL_INFO, 'Successfully created new resource: ' . $resourceProperties['pagetitle'], __METHOD__, __LINE__);
    } else {
        $modx->log(MODX::LOG_LEVEL_ERROR, 'Failed to create new resource: ' . $resourceProperties['pagetitle'] . ". Errors:\n" . implode("\n", $response->getAllErrors()), __METHOD__, __LINE__);
    }
}

// Update resource if ID can be determined (not the case in quick edit!)
if ($properties['Resource_id'] && $object->get('Resource_id')) {
    $resourceProperties['id'] = $object->get('Resource_id');

    // Some settings should not be overwritten with default value
    unset($resourceProperties['class_key']);
    unset($resourceProperties['template']);

    // Run, Forest!
    $response = $modx->runProcessor('resource/update', $resourceProperties);
    if (!$response->isError()) {
        $modx->log(MODX::LOG_LEVEL_INFO, 'Successfully updated resource: ' . $resourceProperties['pagetitle'], __METHOD__, __LINE__);
    } else {
        $modx->log(MODX::LOG_LEVEL_ERROR, 'Failed to update resource: ' . $resourceProperties['pagetitle'] . ". Errors:\n" . implode("\n", $response->getAllErrors()), __METHOD__, __LINE__);
    }
}

// Set forest ID
if ($resourceID && !$object->get('forest_id')) {
    $object->set('forest_id', $resourceID);
}

$object->save();

$earthbrain->resetNull($object, $properties);
$earthbrain->saveLocation($object, $properties, $locationID);

return json_encode($result);