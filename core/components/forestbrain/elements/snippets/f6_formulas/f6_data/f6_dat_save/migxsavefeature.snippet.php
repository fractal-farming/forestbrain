<?php
/**
 * migxSaveFeature
 *
 * @var modX $modx
 * @var array $scriptProperties
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;

$object = $modx->getOption('object', $scriptProperties);
$properties = $modx->getOption('scriptProperties', $scriptProperties, []);
$configs = $modx->getOption('configs', $properties, '');
$postValues = $modx->getOption('postvalues', $scriptProperties, []);

if (!is_object($object)) return;

$result = [];
$locationID = $object->get('location_id');

$earthbrain->resetNull($object, $properties);
$earthbrain->saveLocation($object, $properties, $locationID);

return json_encode($result);