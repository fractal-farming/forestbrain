<?php
/**
 * forestMapPrepareForests
 *
 * Modify field values before the pdoResources snippet in forestMapGetForests is
 * executed.
 *
 * Please note that the final output is a GeoJSON object, so each field needs to
 * generate valid JSON.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var array $row
 */

$corePath = $modx->getOption('forestbrain.core_path', null, $modx->getOption('core_path') . 'components/forestbrain/');
$forestbrain = $modx->getService('forestbrain','ForestBrain',$corePath . 'model/forestbrain/', array('core_path' => $corePath));
$corePath = $modx->getOption('earthbrain.core_path', null, $modx->getOption('core_path') . 'components/earthbrain/');
$earthbrain = $modx->getService('earthbrain','EarthBrain',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthimage = $modx->getService('earthimage','earthImage',$corePath . 'model/earthbrain/', array('core_path' => $corePath));
$earthlocation = $modx->getService('earthlocation','earthLocation',$corePath . 'model/earthbrain/',array('core_path' => $corePath));
$corePath = $modx->getOption('romanescobackyard.core_path', null, $modx->getOption('core_path') . 'components/romanescobackyard/');
$romanesco = $modx->getService('romanesco','Romanesco',$corePath . 'model/romanescobackyard/',array('core_path' => $corePath));

if (!($forestbrain instanceof ForestBrain)) return;
if (!($earthbrain instanceof EarthBrain)) return;
if (!($earthimage instanceof earthImage)) return;
if (!($earthlocation instanceof earthLocation)) return;
if (!($romanesco instanceof Romanesco)) return;

$tplPopupContent = $modx->getOption('tplPopupContent', $scriptProperties, '');

// Coordinates
// =============================================================================

// Randomly generate nearby coordinates to obfuscate real location
$lat = $row['lat'] ?? null;
$lng = $row['lng'] ?? null;
$radius = $row['radius'] ?? null;
if ($lat && $lng && $radius > 0) {
    $randomLocation = array();
    $randomLocation = $earthlocation->obfuscateCoordinates(array($lat,$lng), $radius);

    $row['lat'] = $randomLocation['lat'];
    $row['lng'] = $randomLocation['lng'];

    $row['message'] = $modx->getChunk('richTextMessage', array(
        'size' => 'small',
        'message_type' => 'info',
        'heading' => $modx->lexicon('earthbrain.map.location_obfuscated_heading'),
        'content' => $modx->lexicon('earthbrain.map.location_obfuscated_content', array('radius'=>$row['radius'])),
    ));
}


// Popup content
// =============================================================================

// Create buttons
$row['button_website'] = '';
$row['button_facebook'] = '';
if ($row['website'] ?? false) {
    $row['button_website'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['website'],
        'icon_class' => 'globe icon',
    ));
}
if ($row['facebook'] ?? false) {
    $row['button_facebook'] = $modx->getChunk('buttonHrefIcon', array(
        'link' => $row['facebook'],
        'icon_class' => 'facebook f icon',
    ));
}

if ($row['image'] ?? $row['forest_global_id'] ?? false) {
    $row['image']['img_path'] = 'uploads/img/forest/' . $row['forest_global_id'] . '/';

    // Create thumbnail with ImagePlus
    $image = $modx->runSnippet('ImagePlus', array(
        'value' => $earthimage->fixSourcePath($row['image']),
        'options' => 'w=600&q=85&zc=1',
    ));

    // Create output
    $row['image'] = $modx->getChunk('earthMapPopupImage', array(
        'image' => $image,
        'alt' => $row['pagetitle'],
    ));
}

// Get plant species
$row['plants'] = $forestbrain->getPlants($row, '', '<br>', 3);

// Get category name
if ($row['category'] ?? false) {
    $rmOption = $modx->getObject('rmOption', array(
        'key' => 'forest_category',
        'alias' => $row['category'],
    ));
    if ($rmOption) {
        $row['category'] = $rmOption->get('name');
    }
}

// Generate links
if ($row['id'] ?? false) {
    $row['link'] = $modx->makeUrl($row['id']);
}

// Format region
if ($row['region'] ?? false) {
    $row['region'] = ', ' . $row['region'];
}

// Use chunk tpl for output
$row['popup_content'] = json_encode(
    $modx->getChunk($tplPopupContent, array(
        'image' => $row['image'] ?? '',
        'message' => $row['message'] ?? '',
        'introtext' => nl2br($row['introtext'] ?? ''),
        'plants' => nl2br($row['plants'] ?? ''),
        'button_website' => $row['button_website'] ?? '',
        'button_facebook' => $row['button_facebook'] ?? '',
    ))
);

return json_encode($row);