--- Select forest ---==||
[[pdoResources?
    &parents=`[[++forestbrain.forest_container_id]]`
    &tpl=`@INLINE [[+pagetitle]]==[[+id]]`
    &outputSeparator=`||`
    &showHidden=`1`
    &showUnpublished=`0`
    &limit=`0`
    &sortBy=`menuindex`
    &sortDir=`ASC`
    &where=`{"template:=":[[++forestbrain.forest_template_id]]}`
]]