[[+firstOption:after=`==||`]][[migxLoopCollection?
    &packageName=`forestbrain`
    &classname=`[[+className]]`
    &tpl=`[[+rowTpl]]`
    &outputSeparator=`||`
    &where=`[
        [[+where:append=`,`]]
        {"deleted:=":0}
    ]`
    &joins=`[ [[+joins]] ]`
    &sortConfig=`[{"sortby":"[[+sortBy]]","sortdir":"[[+sortDir]]"}]`
]]