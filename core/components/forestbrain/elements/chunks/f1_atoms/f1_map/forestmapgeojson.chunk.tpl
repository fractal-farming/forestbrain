{
    "type": "Feature",
    "properties": {
        "name": "[[+pagetitle]]",
        "amenity": "",
        "popupContent": [[+popup_content:empty=`""`]],
        "id": "forest-[[+id]]"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [ [[+lng]],[[+lat]] ]
    }
}