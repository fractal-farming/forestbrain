{
    "type": "Feature",
    "properties": {
        "name": "[[+pagetitle]]",
        "amenity": "",
        "popupContent": [[+popup_content]],
        "id": "component-[[+id]]"
    },
    [[+geometry]]
}