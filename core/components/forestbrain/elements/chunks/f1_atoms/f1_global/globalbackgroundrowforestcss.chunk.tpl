[[#[[+resource_id]].alias:toPlaceholder=`alias`]]
[[+img_portrait:replace=`"source": 100001=="source": 1`:replace=`"src": "=="src": "/uploads/img/forest/[[+resource_id]]/overview/`:toPlaceholder=`img_portrait`]]
[[+img_square:replace=`"source": 100001=="source": 1`:replace=`"src": "=="src": "/uploads/img/forest/[[+resource_id]]/overview/`:toPlaceholder=`img_square`]]
[[+img_landscape:replace=`"source": 100001=="source": 1`:replace=`"src": "=="src": "/uploads/img/forest/[[+resource_id]]/overview/`:toPlaceholder=`img_landscape`]]
[[+img_wide:replace=`"source": 100001=="source": 1`:replace=`"src": "=="src": "/uploads/img/forest/[[+resource_id]]/overview/`:toPlaceholder=`img_wide`]]
[[+img_pano:replace=`"source": 100001=="source": 1`:replace=`"src": "=="src": "/uploads/img/forest/[[+resource_id]]/overview/`:toPlaceholder=`img_pano`]]

.background.[[+alias]]:before {
    background:
        url('[[ImagePlus? &value=`[[+img_portrait:empty=`[[+img_landscape]]`]]` &options=`w=500` &type=`thumb`]]')
        center 30% / cover
        no-repeat
        scroll
    ;
    opacity: .75;
}
.background.[[+alias]]:after {
    background: #000 !important;
}

@media (min-width: 500px) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_square:empty=`[[+img_landscape]]`]]` &options=`w=800` &type=`thumb`]]')
            center center / cover
        ;
    }
}
@media (min-width: 800px) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_wide:empty=`[[+img_landscape]]`]]` &options=`w=1300` &type=`thumb`]]')
            center 30% / cover
        ;
    }
}
@media (min-width: 1300px) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_pano:empty=`[[+img_wide]]`]]` &options=`w=1900` &type=`thumb`]]')
            center 30% / cover
        ;
    }
}

[[++img_hidpi:eq=`1`:then=`
@media
(-webkit-min-device-pixel-ratio: 2),
(min-resolution: 192dpi) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_square:empty=`[[+img_landscape]]`]]` &options=`w=800` &type=`thumb`]]')
            center center / cover
        ;
    }
}
@media
(-webkit-min-device-pixel-ratio: 2) and (min-width: 800px),
(min-resolution: 192dpi) and (min-width: 800px) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_wide:empty=`[[+img_landscape]]`]]` &options=`w=1600` &type=`thumb`]]')
            center 30% / cover
        ;
    }
}
@media
(-webkit-min-device-pixel-ratio: 2) and (min-width: 1300px),
(min-resolution: 192dpi) and (min-width: 1300px) {
    .background.[[+alias]]:before {
        background:
            url('[[ImagePlus? &value=`[[+img_pano:empty=`[[+img_wide]]`]]` &options=`w=2600` &type=`thumb`]]')
            center 30% / cover
        ;
    }
}
`]]
