[[+Need_title]]
<span style="float:right;clear:right;">
    [[+Need_published:isnot=`1`:then=`
    <i class="icon icon-fw icon-lock" title="Not visible to others"></i>
    `:else=`
    <i class="icon icon-fw"></i>
    `]]
    [[+Need_priority:eq=`3`:then=`
    <i class="icon icon-fw icon-heartbeat" title="Need"></i>
    `]]
    [[+Need_priority:eq=`2`:then=`
    <i class="icon icon-fw icon-heart-o" title="Want"></i>
    `]]
    [[+Need_priority:lte=`1`:then=`
    <i class="icon icon-fw icon-gift" title="Wish"></i>
    `]]
</span>