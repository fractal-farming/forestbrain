[[+Resource_pagetitle:notempty=`
[[+Resource_pagetitle]]
<i class="icon icon-right icon-fw icon-cube" title="Component ([[+id]])"></i>
`]]
[[+Taxon_id:notempty=`
[[+Taxon_name:empty=`[[+Taxon_name_local]]`]] [[+Taxon_category]]
<i class="icon icon-right icon-fw icon-leaf" title="Plant ([[+id]])"></i>
`]]
<i class="icon icon-right icon-fw icon-info-circle"
   title="[[+Output_availability:empty=`unknown`:ucfirst:append=` availability`]], [[+Output_quality:empty=`unknown`:append=` quality`]]">
</i>