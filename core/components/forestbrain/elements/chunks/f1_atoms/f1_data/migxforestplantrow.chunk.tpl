[[+quantity]]
[[+stage:in=`seed,seedling`:then=`[[+stage]][[+quantity:gt=`1`:then=`s`]]`:else=`[[+stage]]`]]
[[migxLoopCollection?
    &packageName=`earthbrain`
    &classname=`earthSource`
    &tpl=`@CODE:<i class="icon icon-right icon-fw icon-info-circle" title="Origin: [[+name]]"></i>`
    &where=`{"id":"[[+source_id]]"}`
]]
<span style="float:right;">
    [[+published:isnot=`1`:then=`<i class="icon icon-lock" title="Not visible to others"></i>`:else=`<i class="icon icon-fw"></i>`]]
    [[+zone:isnot=``:then=`<i class="icon icon-bullseye" title="Zone: [[+zone]]"></i>`:else=`<i class="icon icon-fw"></i>`]]
    [[+location_id:isnot=``:then=`<i class="icon icon-map-marker" title="Has location"></i>`:else=`<i class="icon icon-fw"></i>`]]
</span>
