[[$migxGridSourceName?
    &parent_id=`[[+Source_parent_id]]`
    &class_key=`[[+Source_class_key]]`
    &name=`[[+Source_name]]`
    &published=`[[+Source_published]]`
    &idx=`[[+id]]`
]]

<i class="icon icon-right icon-fw icon-info-circle"
   title="[[+availability:empty=`unknown`:ucfirst:append=` availability`]], [[+quality:empty=`unknown`:append=` quality`]]">
</i>