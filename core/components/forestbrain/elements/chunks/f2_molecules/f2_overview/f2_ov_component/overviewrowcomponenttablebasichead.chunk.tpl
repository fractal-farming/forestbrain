<thead>
<tr>
    <th>[[%forestbrain.component.heading]]</th>
    <th>[[%forestbrain.component.inputs_heading]]</th>
    <th>[[%forestbrain.component.outputs_heading]]</th>
    <th>[[%forestbrain.component.data_stage]]</th>
    <th>[[%forestbrain.component.data_zone]]</th>
    [[-<th>[[%forestbrain.component.admin]]</th>]]
</tr>
</thead>
