<td data-title="[[%forestbrain.forest.heading]]">
    [[+img_landscape:notempty=`
    <figure class="ui image">
        <img class="ui mini image" src="[[ImagePlus? &value=`[[+image_json]]` &options=`w=200&zc=1` &type=`thumb`]]" alt="[[+pagetitle]]">
    </figure>
    `]]
    [[+pagetitle]]<br>
    <em class="meta">[[+locality:append=`, `]][[+region]]</em>
</td>

<td data-title="[[%forestbrain.forest.trees]]">
    [[+plants]]
</td>

<td data-title="[[%forestbrain.forest.person]]">
    [[+person]]
</td>

<td data-title="[[%forestbrain.forest.admin]]">
    [[+admin]]
</td>

[[-
<td>
    <a href="[[~[[+id]]]]" class="ui compact primary button">
        [[%forestbrain.map.button_read_more]]
    </a>
</td>
]]