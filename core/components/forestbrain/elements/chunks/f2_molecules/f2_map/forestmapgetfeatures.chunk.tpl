[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/features/[[+user_access_level]]``]]
    &class=`forestFeature`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`forestMapFeatureGeoJSON`
    &tplWrapper=`earthMapWrapperGeoJSON`
    &where=`[
        [[+forest_id:notempty=`
        {"forest_id:=":[[+forest_id]]},
        `]]
        {"published:=":1}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Location": {
            "class": "earthLocation",
            "on": "forestFeature.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "forestFeature.createdby = CreatedBy.internalKey"
        }
    }`
    &select=`{
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.geojson AS geojson",
        "CreatedBy": "CreatedBy.fullname AS admin",
        "forestFeature": "id,title,description,type,published,publishedon"
    }`

    &tplPopupContent=`forestMapPopupContentFeature`
    &prepareSnippet=`forestMapPrepareFeatures`

    &showLog=`0`
]]