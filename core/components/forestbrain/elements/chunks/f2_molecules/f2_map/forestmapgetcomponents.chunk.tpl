[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/components/[[+user_access_level]]``]]
    &class=`forestComponent`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`forestMapComponentGeoJSON`
    &tplWrapper=`earthMapWrapperGeoJSON`
    &where=`[
        [[+forest_id:notempty=`
        {"forest_id:=":[[+forest_id]]},
        `]]
        {"Location.lat:!=":""},
        {"Location.lng:!=":""},
        {"deleted:=":0}
    ]`
    &outputSeparator=`,`
    &sortby=`id`

    &leftJoin=`{
        "Resource": {
            "class": "modResource",
            "on": "forestComponent.resource_id = Resource.id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "forestComponent.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "forestComponent.createdby = CreatedBy.internalKey"
        }
    }`
    &select=`{
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.geojson AS geojson",
        "CreatedBy": "CreatedBy.fullname AS admin",
        "Resource": "Resource.pagetitle as title",
        "forestComponent": "id,type,stage,zone"
    }`

    &tplPopupContent=`forestMapPopupContentComponent`
    &prepareSnippet=`forestMapPrepareComponents`

    &showLog=`0`
]]