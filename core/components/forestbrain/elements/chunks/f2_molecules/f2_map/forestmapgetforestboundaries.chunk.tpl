[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/forests/[[+user_access_level]]``]]

    &parents=`[[+parents:default=`-1`]]`
    &resources=`[[+forest_id]]`
    &context=`[[+context:default=`web`]]`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`@INLINE [[+geojson]]`
    &tplWrapper=`earthMapWrapperGeoJSON`
    &outputSeparator=`,`
    &where=`[
        {"template:IN":[100001,100004]},
        {"Location.geojson:!=":""},
        {"published:=":1}
    ]`
    &showHidden=`1`

    &leftJoin=`{
        "Data": {
            "class": "forestData",
            "on": "modResource.id = Data.resource_id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "Data.location_id = Location.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "modResource.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`modResource.id`
    &select=`{
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.elevation AS elevation, Location.geojson AS geojson",
        "CreatedBy": "CreatedBy.fullname AS admin",
        "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
    }`

    &prepareSnippet=`forestMapPrepareFeatures`

    &showLog=`0`
]]