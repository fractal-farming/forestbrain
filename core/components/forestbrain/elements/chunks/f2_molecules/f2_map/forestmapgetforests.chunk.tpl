[[!getCache?
    &element=`pdoResources`
    [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/forests/[[+user_access_level]]``]]

    &parents=`[[+parents:default=`[[*parent]]`]]`
    &resources=`[[+resources]]`
    &context=`[[+context:default=`[[*context_key]]`]]`

    &depth=`0`
    &limit=`0`
    &offset=`0`
    &tpl=`forestMapGeoJSON`
    &tplWrapper=`earthMapWrapperGeoJSON`
    &outputSeparator=`,`
    &where=`[
        {"template:IN":[100001,100004]},
        {"Location.lat:!=":""},
        {"Location.lng:!=":""},
        {"published:=":1}
    ]`
    &includeTVs=`
        forest_global_id,
        overview_img_landscape,
        overview_img_portrait,
        overview_img_square,
        overview_img_wide,
        overview_img_pano,
        overview_link_text
    `
    &processTVs=`0`
    &tvFilters=`[[*context_key:isnot=`web`:then=`forest_global_id>>0`]]`
    &tvPrefix=``
    &showHidden=`1`

    &leftJoin=`{
        "GlobalTV": {
            "class": "modTemplateVarResource",
            "on": "modResource.id = GlobalTV.contentid AND GlobalTV.tmplvarid = 100012"
        },
        "Data": {
            "class": "forestData",
            "on": "GlobalTV.value = Data.resource_id"
        },
        "Location": {
            "class": "earthLocation",
            "on": "Data.location_id = Location.id"
        },
        "Address": {
            "class": "earthAddress",
            "on": "Data.address_id = Address.id"
        },
        "Image": {
            "class": "earthImage",
            "on": "Data.image_id = Image.id"
        },
        "CreatedBy": {
            "class": "modUserProfile",
            "on": "modResource.createdby = CreatedBy.internalKey"
        }
    }`
    &groupby=`modResource.id`
    &select=`{
        "Data": "Data.category AS category, Data.size AS size",
        "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng, Location.radius AS radius",
        "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
        "Image": "Image.img_wide as image",
        "CreatedBy": "CreatedBy.fullname AS admin",
        "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
    }`

    &tplPopupContent=`forestMapPopupContent`
    &prepareSnippet=`forestMapPrepareForests`

    &showLog=`0`
]]