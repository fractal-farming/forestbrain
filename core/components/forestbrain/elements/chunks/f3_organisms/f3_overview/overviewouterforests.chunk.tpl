[[setBoxType? &input=`[[+row_tpl]]` &prefix=`fo_[[+layout_id]]_[[+unique_idx]]`]]
[[$overviewSettingsPrepare? &uid=`[[+prefix]]`]]

[[+pagination:eq=`1`:then=`
<div id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`fo__` &then=`fo_[[Time]]` &else=`[[+prefix]]`]]" class="pagination-wrapper">
`]]
    <div class="ui [[+cols]] [[+[[+prefix]].grid_settings]] [[+responsive:replace=`,== `]] [[+padding]] nested overview [[+[[+prefix]].box_type]]">
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/forests/[[+user_access_level]]``]]

        &parents=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`0` &else=`[[++forestbrain.forest_container_id]]`]]`
        &resources=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`[[+resources]]` &else=`null`]]`

        &depth=`99`
        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowForest[[+[[+prefix]].row_type]]`
        &where=`{"template:IN":[100001,100004]}`
        &includeTVs=`
            forest_global_id
        `
        &processTVs=`0`
        &tvFilters=`[[*context_key:isnot=`forestbrain`:then=`forest_global_id>>0`]]`
        &tvPrefix=``
        &showHidden=`[[+show_hidden:default=`1`]]`

        &leftJoin=`{
            "GlobalTV": {
                "class": "modTemplateVarResource",
                "on": "modResource.id = GlobalTV.contentid AND GlobalTV.tmplvarid = 100012"
            },
            "Data": {
                "class": "forestData",
                "on": "GlobalTV.value = Data.resource_id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "Data.location_id = Location.id"
            },
            "Address": {
                "class": "earthAddress",
                "on": "Data.address_id = Address.id"
            },
            "Image": {
                "class": "earthImage",
                "on": "Data.image_id = Image.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "modResource.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`modResource.id`
        &select=`{
            "Data": "Data.category AS category, Data.size AS size",
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "Image": "Image.path AS img, Image.img AS img_free, Image.img_landscape AS img_landscape, Image.img_portrait AS img_portrait, Image.img_square AS img_square, Image.img_wide AS img_wide, Image.img_pano AS img_pano",
            "CreatedBy": "CreatedBy.fullname AS admin",
            "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
        }`
        &showLog=`0`

        &prepareSnippet=`overviewPrepareForests`

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </div>
[[[[+pagination:eq=`1`:then=`$paginationFluid:append=`
</div>
`? &prefix=`[[+prefix]]``]]]]
