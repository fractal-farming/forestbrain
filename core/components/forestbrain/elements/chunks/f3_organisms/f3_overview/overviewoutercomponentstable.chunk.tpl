[[setBoxType? &input=`[[+row_tpl]]` &prefix=`ct_[[+layout_id]]_[[+unique_idx]]`]]

[[$overviewSettingsPrepare? &uid=`[[+prefix]]`]]

<table id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`ct__` &then=`ct_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/components/[[+user_access_level]]``]]

        &parents=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`0` &else=`[[+parent]]`]]`
        &resources=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`[[+resources]]` &else=`null`]]`

        &depth=`0`
        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowComponent[[+[[+prefix]].row_type]]`
        &where=`{"template":"[[++forestbrain.component_template_id]]"}`
        &includeTVs=``
        &processTVs=`0`
        &tvFilters=``
        &tvPrefix=``
        &showHidden=`[[+show_hidden:default=`1`]]`

        &leftJoin=`{
            "Data": {
                "class": "forestComponent",
                "on": "modResource.id = Data.resource_id"
            },
            "Forest": {
                "class": "forestData",
                "on": "Data.forest_id = Forest.resource_id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "Data.location_id = Location.id"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "modResource.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`modResource.id`
        &select=`{
            "Data": "Data.id AS component_id, Data.zone AS zone, Data.type AS type, Data.stage AS stage",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "CreatedBy": "CreatedBy.fullname AS admin",
            "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
        }`
        &showLog=`0`

        &prepareSnippet=`overviewPrepareComponents`

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
