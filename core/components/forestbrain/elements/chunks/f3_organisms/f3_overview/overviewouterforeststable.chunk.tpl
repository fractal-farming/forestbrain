[[setBoxType? &input=`[[+row_tpl]]` &prefix=`ft_[[+layout_id]]_[[+unique_idx]]`]]

[[$overviewSettingsPrepare? &uid=`[[+prefix]]`]]

<table id="[[If? &subject=`[[+prefix]]` &operator=`is` &operand=`ft__` &then=`ft_[[Time]]` &else=`[[+prefix]]`]]"
       class="ui [[+[[+prefix]].grid_settings]] [[+padding:replace=`relaxed==padded`]] overview [[+[[+prefix]].box_type]]"
    >
    [[$[[+row_tpl]]Head?
        &db_fields=`[[+[[+prefix]].dataset_content]]`
    ]]
    <tbody>
    [[![[If? &subject=`[[+pagination]]` &operator=`EQ` &operand=`1` &then=`pdoPage` &else=`getCache`]]?
        &element=`pdoResources`
        [[++custom_cache:eq=`1`:then=`&cacheKey=`custom/forests/[[+user_access_level]]``]]

        &parents=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`0` &else=`[[++forestbrain.forest_container_id]]`]]`
        &resources=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`[[+resources]]` &else=`null`]]`

        &depth=`0`
        &limit=`[[+limit:default=`0`]]`
        &offset=`[[+offset:default=`0`]]`
        &tpl=`overviewRowForest[[+[[+prefix]].row_type]]`
        &where=`{"template:IN":[100001,100004]}`
        &includeTVs=`
            forest_global_id,
            overview_img_landscape,
            overview_img_portrait,
            overview_img_square,
            overview_img_wide,
            overview_img_pano,
            overview_link_text
        `
        &processTVs=`0`
        &tvFilters=`[[*context_key:isnot=`forestbrain`:then=`forest_global_id>>0`]]`
        &tvPrefix=``
        &showHidden=`[[+show_hidden:default=`1`]]`

        &leftJoin=`{
            "GlobalTV": {
                "class": "modTemplateVarResource",
                "on": "modResource.id = GlobalTV.contentid AND GlobalTV.tmplvarid = 100012"
            },
            "Data": {
                "class": "forestData",
                "on": "GlobalTV.value = Data.resource_id"
            },
            "Location": {
                "class": "earthLocation",
                "on": "Data.location_id = Location.id"
            },
            "Address": {
                "class": "earthAddress",
                "on": "Data.address_id = Address.id"
            },
            "Person": {
                "class": "modUserProfile",
                "on": "Data.person_id = Person.internalKey"
            },
            "CreatedBy": {
                "class": "modUserProfile",
                "on": "modResource.createdby = CreatedBy.internalKey"
            }
        }`
        &groupby=`modResource.id`
        &select=`{
            "Data": "Data.category AS category, Data.size AS size",
            "Location": "Location.id AS location, Location.lat AS lat, Location.lng AS lng",
            "Address": "Address.id AS address, Address.locality AS locality, Address.region AS region",
            "Person": "Person.fullname AS person",
            "CreatedBy": "CreatedBy.fullname AS admin",
            "modResource": "id,pagetitle,longtitle,menutitle,introtext,publishedon"
        }`
        &showLog=`0`

        &prepareSnippet=`overviewPrepareForests`
        &tplPlantRow=`earthTaxonBasic`

        &sortby=`[[If? &subject=`[[+resources]]` &operator=`notempty` &then=`FIELD(modResource.id, [[+resources]])` &else=`[[+sortby]]`]]`
        &sortdir=`[[+[[+prefix]].sortdir]]`

        [[$overviewSettings? &uid=`[[+prefix]]`]]
        [[[[+pagination:eq=`1`:then=`$overviewSettingsPagination? &uid=`[[+prefix]]``]]]]
    ]]
    </tbody>
    <tfoot class="pagination-table">
        [[+pagination:eq=`1`:then=`[[!+[[+prefix]].page.nav]]`]]
    </tfoot>
</table>

[[loadAssets? &component=`table`]]
