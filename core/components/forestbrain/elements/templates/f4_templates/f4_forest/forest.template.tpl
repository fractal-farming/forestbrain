<!DOCTYPE html>
<html id="[[*context_key]]" lang="[[++cultureKey]]">

<head>
    [[[[modifiedIf?
        &subject=`headTheme`
        &operator=`iselement`
        &operand=`chunk`
        &then=`$headTheme`
        &else=`$head`
    ]]]]
</head>

[[migxLoopCollection?
    &packageName=`forestbrain`
    &classname=`forestData`
    &where=`[{"resource_id":"[[*forest_global_id:empty=`[[*id]]`]]"}]`
    &joins=`[{"alias":"Address"},{"alias":"Location"}]`
    &toJsonPlaceholder=`forestData`
]]
[[migxJsonToPlaceholders?
    &value=`[[+forestData]]`
    &prefix=`data.`
]]

[[migxLoopCollection?
    &packageName=`forestbrain`
    &classname=`forestPlant`
    &joins=`[{"alias":"Taxon"}]`
    &tpl=`earthTaxonLabel`
    &where=`[{"forest_id":"[[*forest_global_id:empty=`[[*id]]`]]"},{"published":1}]`
    &sortConfig=`[{"sortby":"name","sortdir":"ASC"}]`
    &toPlaceholder=`forest_plants`
]]

<body id="[[*alias]]" class="overview forest">

[[$offCanvasNav]]

<div class="pusher">
    <header id="header" class="masthead [[++navbar_sticky:eq=`0`:then=`non-stick`]]">
        [[[[If?
            &subject=`[[$mainNavTheme]]`
            &operator=`isnull`
            &then=`$mainNav`
            &else=`$mainNavTheme`
        ]]]]

        <div id="hero" class="ui vertical stripe segment background [[*alias]]">
            <div class="ui grid container">
                <div class="sixteen wide mobile twelve wide tablet ten wide computer column">
                    <h1 class="ui inverted header">[[*pagetitle]]</h1>
                </div>
            </div>
        </div>
    </header>

    <main id="main" role="main">
        <article id="content">
            <div class="ui vertical stripe segment white">
                <div class="ui container">
                    <div class="ui stackable on tablet very relaxed equal width grid">
                        <div class="ten wide column">
                            <section id="forest-intro">
                                [[*introtext:replace=`<p>==<p class="lead">`]]
                            </section>

                            [[+forest_plants:notempty=`
                            <div class="ui section divider"></div>

                            <section id="forest-plants">
                                <div class="ui labels">
                                    [[+forest_plants]]
                                </div>
                            </section>
                            `]]
                        </div>
                        <div class="column">
                            <aside id="forest-connect">
                                [[+data.website:notempty=`<a href="[[+data.website]]" class="ui basic massive circular icon button" target="_blank" title="Visit website"><i class="globe icon"></i></a>`]]
                                [[+data.facebook:notempty=`<a href="[[+data.facebook]]" class="ui basic massive circular icon button" target="_blank" title="Visit Facebook page"><i class="facebook f icon"></i></a>`]]
                                [[+data.email:notempty=`<a href="mailto:[[+data.email]]" class="ui basic massive circular icon button" target="_blank" title="Send an email"><i class="envelope icon"></i></a>`]]
                            </aside>
                        </div>
                    </div>
                </div>
            </div>

            [[*content]]

            <div class="ui vertical stripe segment inverted primary-color">
                <div class="ui container">
                    <div class="ui stackable on tablet very relaxed equal width grid">

                        <div id="forest-details" class="seven wide computer six wide large screen column">
                            <h2 class="ui small header">[[%forestbrain.forest.data_heading]]</h2>

                            <table class="ui large compact very basic unstackable inverted table">
                                <tr>
                                    <td>[[%forestbrain.forest.data_category]]</td>
                                    <td>[[+data.category:renderInputOption=`alias`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%forestbrain.forest.data_size]]</td>
                                    <td>[[+data.size:append=` hectare`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%forestbrain.forest.data_age]]</td>
                                    <td>[[+data.date_start:ago=`%Y`:stripString=` ago`]]</td>
                                </tr>
                                <tr>
                                    <td>[[%forestbrain.forest.data_access]]</td>
                                    <td>[[+data.access:renderInputOption=`alias`]]</td>
                                </tr>
                                <tr>
                                    <td class="top aligned">[[%forestbrain.address.heading]]</td>
                                    <td>
                                        [[+data.Address_line_1:append=`<br>`]]
                                        [[+data.Address_line_2:append=`<br>`]]
                                        [[+data.Address_line_3:append=`<br>`]]
                                        [[+data.Address_locality]][[+data.Address_region:prepend=`, `]]
                                    </td>
                                </tr>
                                <tr>
                                    <td>[[%forestbrain.location.elevation]]</td>
                                    <td>[[+data.Location_elevation:append=` meters`]]</td>
                                </tr>
                            </table>

                            [[+data.website:notempty=`<a href="[[+data.website]]" class="ui basic button" target="_blank"><i class="globe icon"></i>[[%forestbrain.forest.data_website]]</a>`]]
                            [[+data.facebook:notempty=`<a href="[[+data.facebook]]" class="ui basic button" target="_blank"><i class="facebook f icon"></i>[[%forestbrain.forest.data_facebook]]</a>`]]
                        </div>

                        <div id="forest-location" class="column">
                            [[$earthMapGeoJSON:toPlaceholder=`forestMapGeoJSON`?
                                &name=`[[*pagetitle]]`
                                &lat=`[[+data.Location_lat]]`
                                &lng=`[[+data.Location_lng]]`
                            ]]
                            [[$earthMapLayerJS:toPlaceholder=`forestMapLayer`?
                                &layer_title=`Forest`
                                &layer_geojson=`[[$earthMapWrapperGeoJSON? &output=`[[+forestMapGeoJSON]]`]]`
                                &show_on_load=`1`
                                &idx=`1`
                            ]]

                            [[$earthMap?
                                &map_id=`forest-map`
                                &forest_id=`[[*id]]`
                                &rows=`[[+forestMapLayer]]`
                                &base_layers=`Terrain,Satellite`
                                &default_layer=`Terrain`
                                &zoom_level_start=`11`
                            ]]
                        </div>
                    </div>
                </div>
            </div>
        </article>

        [[*neighbors_visibility:eq=`1`:then=`
        <nav id="menu-neighbors" class="ui large fluid two item menu">
            [[pdoNeighbors?
                &loop=`1`
                &tplPrev=`neighborNavItemPrev`
                &tplNext=`neighborNavItemNext`
                &tplWrapper=`@INLINE [[+prev]][[+next]]`
                &sortby=`publishedon`
                &sortdir=`asc`
            ]]
        </nav>
        `]]
    </main>

    [[[[modifiedIf?
        &subject=`footerTheme`
        &operator=`iselement`
        &operand=`chunk`
        &then=`$footerTheme`
        &else=`$footer`
    ]]]]
</div>

[[loadAssets? &component=`map`]]
[[loadAssets? &component=`table`]]
[[$script]]

</body>
</html>