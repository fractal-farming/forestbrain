// Generate password during registration
$('#generate-password').click(function() {
    const password = generatePassword(13);

    // Show password in modal
    $('.password.modal')
        .modal({
            onApprove: function() {
                // Set password in form fields
                $('#set-password input[type="password"]')
                    .empty()
                    .val(password)
                ;
            }
        })
        .modal('show')
        .find('.password.header')
        .empty()
        .append(password)
    ;
});

$('#set-password')
    .form({
        fields: {
            password: {
                identifier: 'password-new',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Please enter a password.'
                    },
                    {
                        type   : 'minLength[8]',
                        prompt : 'Password is too short! Please specify a password of at least {ruleValue} characters.'
                    }
                ]
            },
            match: {
                identifier: 'password-new-confirm',
                rules: [
                    {
                        type   : 'match[password-new]',
                        prompt : ' Passwords do not match.'
                    }
                ]
            },
        }
    })
;